resource "aws_vpc" "default" {
	cidr_block = "${var.vpc_cidr}"
	enable_dns_hostnames = true

	tags {
		Name = "test-vpc"
	}
}

resource "aws_subnet" "public" {
	count = "${length(var.availability_zone)}"
	vpc_id = "${aws_vpc.default.id}"
	cidr_block = "${cidrsubnet(var.vpc_cidr, 8, count.index)}"
	availability_zone = "${element(var.availability_zone, count.index)}"
	tags {
		Name = "Public Subnet - ${element(var.availability_zone, count.index)}"
	}
}

resource "aws_subnet" "private" {
	count = "${length(var.availability_zone)}"
	vpc_id = "${aws_vpc.default.id}"
	cidr_block = "${cidrsubnet(var.vpc_cidr, 8, count.index+2)}"
	availability_zone = "${element(var.availability_zone, count.index)}"
	tags {
		Name = "Private Subnet - ${element(var.availability_zone, count.index)}"
	}
}


/*
Internet gateway
*/
resource "aws_internet_gateway" "gw" {
	vpc_id = "${aws_vpc.default.id}"

	tags {
		Name = "VPC IGW"
	}
}

/*
NAT Gateway Configuration
*/
resource "aws_eip" "nat_eip" {
	vpc = true
	depends_on = ["aws_internet_gateway.gw"]
}

resource "aws_nat_gateway" "nat" {
	allocation_id = "${aws_eip.nat_eip.id}"
	subnet_id = "${element(aws_subnet.public.*.id, 0)}"
	depends_on = ["aws_internet_gateway.gw"]
}


/*
Public Route Configuration
*/
resource "aws_route_table" "public" {
	vpc_id = "${aws_vpc.default.id}"

	route {
		cidr_block = "0.0.0.0/0"
		gateway_id = "${aws_internet_gateway.gw.id}"
	}

	tags {
		Name = "Public Subnet Route Table"
	}
}

/*
Private Subnet Configuration
*/
resource "aws_route_table" "private" {
	vpc_id = "${aws_vpc.default.id}"

	route {
		cidr_block = "0.0.0.0/0"
		gateway_id = "${aws_nat_gateway.nat.id}"
	}

	tags {
		Name = "Private Subnet Route Table"
	}
}

resource "aws_route_table_association" "public" {
	count = "${length(var.availability_zone)}"
	subnet_id = "${element(aws_subnet.public.*.id, count.index)}"
	route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table_association" "private" {
	count = "${length(var.availability_zone)}"
	subnet_id = "${element(aws_subnet.private.*.id, count.index)}"
	route_table_id = "${aws_route_table.private.id}"
}
