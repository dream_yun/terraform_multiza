# variables.tf
variable "aws_region" {
	description = "Region for VPC"
	default = "ap-northeast-2"
}

variable "vpc_cidr" {
	description = "CIDR for JOINC"
	default = "10.100.0.0/16"
}

variable "availability_zone" {
	description = "Seoul region availability zone"
	type = "list"
	default = ["ap-northeast-2a", "ap-northeast-2b"]
}

/*
variable "key_path" {
	description = "SSH Public key path"
	default = "/home/yundream/.ssh/id_rsa.pub"
}
*/

variable "ami" {
	description = "Amazon Linux AMI"
	default = "ami-047f7b46bd6dd5d84"
}
